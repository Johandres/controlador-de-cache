----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.03.2019 17:28:44
-- Design Name: 
-- Module Name: CorrimientoIzqDer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CorrimientoIzqDer1 is
Generic(nbits:integer:=4);--n bits es eln�mero de bits del sumador
PORT(a,b:in std_logic_vector(nbits downto 1);
ALUFLAG1:in std_logic;
s,s1:out std_logic_vector(nbits downto 1);
c,c1:out std_logic);
end CorrimientoIzqDer1;

architecture Behavioral of CorrimientoIzqDer1 is
signal m,n:std_logic;
signal G,P:std_logic_vector(nbits downto 1);
Begin
        process
        variable b1:integer:=1;
        begin
        b1:=to_integer(unsigned(b));
            m <= a(nbits-b1+1);
            n <= a((b1-1));
            for j in 1 to (nbits-b1) loop
                G(j+b1) <= a(j);
                P(nbits-b1-j+1)<=a(nbits-j+1);
                end loop; 
            for i in 1 to b1 loop
                P(nbits-i+1)<=ALUFLAG1;
                G(i)<=ALUFLAG1;
                End loop;
        wait;
     end process;
 s <= G;
 s1<= P;
 c <= m;
 c1 <= n;
end Behavioral;

