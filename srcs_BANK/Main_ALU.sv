`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.03.2019 13:36:12
// Design Name: 
// Module Name: Main_ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Main_ALU #(parameter nbits=4)(
input logic [3:0] seleccion,
input logic [nbits-1 :0] a,
input logic [nbits-1 :0] b,
input logic ALUFLAG1,
output wire [6 :0] y,
output logic N,Z,C,V,
output logic dp,
output logic [7:0] an,
input wire Sign            //N=negative flag; Z=zero flag; C=carry out; V=overflowflag;
    );
logic [nbits-1:0] s;
logic [nbits-1 :0] s1,s2,s3,s4,s5,s6,s7,s8,s9,s10;
logic c,c1,c2,c3,c5,c6,signwire,cwire,awire,bwire,bcwire,swire;
logic [6:0] c4;
logic [nbits-1:0] Swire;
CarryLookAhead #(nbits) Suma(a,b,ALUFLAG1,s1,c);
RestadorCarryLockahead #(nbits) Resta(ALUFLAG1,a,b,c1,s2,bcwire);
IncrementardecrementarOperando #(nbits) Incrementar(a,b,ALUFLAG1,s4,s3,c5,c6);
LogicalFunc #(nbits) Logicas(a,b,ALUFLAG1,s5,s6,s8,s7);
Corrimiento #(nbits) Corrimientos(a,b,ALUFLAG1,c2,c3,s10,s9);
Selector #(nbits) Selector1(s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,c,c1,c2,c3,c5,c6,bcwire,ALUFLAG1,seleccion,a,b,Sign,s,Swire,C,signwire,cwire,awire,bwire,swire);
NegativeFlag NFlag(N,awire,bwire,swire,signwire);  
OverflowFlag VFlag(V,awire,bwire,swire,signwire);  
ZeroFlag #(nbits) ZFlag(Z,Swire,cwire,signwire);
Decoder7segment Decotest(s,c4);
Inversor #(7)Inversotest(c4,y);
always_comb
begin
if(seleccion==0)
    begin
    an=8'b00000001;
    dp=0;  
    end
else
    begin
    dp=1;
    an=8'b11111110;
    end
end
endmodule

