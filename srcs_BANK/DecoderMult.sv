`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.04.2019 18:10:48
// Design Name: 
// Module Name: DecoderMult
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MainFloatTB(
    );
logic [31:0] NumA;
logic [31:0] NumB;
logic [7:0] Dezplazamiento;
logic [22:0] s,s1,s2;
logic [31:0] Resultado;
Main_SumaFloat MainTest(.NumA(NumA),.NumB(NumB),.Dezplazamiento(Dezplazamiento),.s(s),.s1(s1),.s2(s2),.Resultado(Resultado));
initial
begin
NumA=32'b01000001100000000000000000000000;
NumB=32'b01000000100000000000000000000000;
end
endmodule
