----------------------------------------------------------------------------------
-- Tecnologico de Costa Rica
-- Johan Chaves Zamora 2016062523
-- 
-- Create Date: 21.02.2019 12:31:13
-- Design Name: 
-- Module Name: FULLADDER - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
entity FULLADDER is
port(a, b, cin: in STD_LOGIC;
s, cout: out STD_LOGIC);
end FULLADDER;

architecture Behavioral of FULLADDER is
signal p, g: STD_LOGIC;
begin
p <= a xor b;
g <= a and b;
s <= p xor cin;
cout <= g or (p and cin);
end Behavioral;
