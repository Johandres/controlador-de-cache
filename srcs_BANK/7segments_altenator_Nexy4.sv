`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.04.2019 22:00:31
// Design Name: 
// Module Name: 7segments_altenator_Nexy4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module altenator_7segments_Nexy4 # (parameter PAR_nsegments=8)( //PAR_nsegments no puede ser mayor
    input CLK,
    input [4*PAR_nsegments-1:0] IN_BCD,         //La entrada va en pares de 8 bits [7:0] 7 segmentos 0, [15:8] 7 segmentos 1
    output logic [3:0] OUT_BCD,                 //recuerde que el bcd est� activo en bajo en nexys4
    output logic [PAR_nsegments:0] OUT_AN                   //recuerde que el AN est� activo en alto en nexys4
    );                                          // ambas condiciones anteriores deben cumplirse para iluminar
    
    initial begin
    OUT_AN  <=8'd1;
    end
    
    always @(posedge CLK)
    begin
    case(OUT_AN)
    1:  begin 
        OUT_AN  <=OUT_AN<<1;
        OUT_BCD <= IN_BCD[7:4];
        end
        
    2:  begin
        OUT_AN  <=OUT_AN<<1;
        OUT_BCD <= IN_BCD[11:8];
        end
        
    4:  begin 
        OUT_AN  <=OUT_AN<<1;
        OUT_BCD <= IN_BCD[15:12];
        end
    
    8:  begin 
        OUT_AN  <=OUT_AN<<1;
        OUT_BCD <= IN_BCD[19:16];
        end
    
    16:  begin 
        OUT_AN  <=OUT_AN<<1;
        OUT_BCD <= IN_BCD[23:20];
        end
    
    32:  begin 
        OUT_AN  <=OUT_AN<<1;
        OUT_BCD <= IN_BCD[27:24];
        end
    
    64:  begin 
        OUT_AN  <=OUT_AN<<1;
        OUT_BCD <= IN_BCD[31:28];
        end
        
    128:  begin 
        OUT_AN  <=OUT_AN<<1;
        OUT_BCD <= IN_BCD[7:4];
        end
        
    256:  begin 
        OUT_AN  <=8'b0000_0001;
        OUT_BCD <= IN_BCD[3:0];
        end  
        
    endcase
end
endmodule
