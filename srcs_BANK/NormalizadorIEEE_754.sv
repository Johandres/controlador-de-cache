`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.04.2019 15:48:59
// Design Name: 
// Module Name: NormalizadorIEEE_754
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Normalizador(
input logic [22:0] Res,
input logic [7:0] NumExpA1,
input logic [7:0] NumExpB1,
input logic select,
output logic [31:0] Resultado
    );
    logic [7:0] NumExpA;
    logic [7:0] NumExpB;
    always_comb
    begin
    NumExpB=NumExpB1;
    NumExpA=NumExpA1;
    if( Res==0)
        begin
            NumExpB=NumExpB1+1;
            NumExpA=NumExpA1+1;
        end
    Resultado=32'b0000000000000000000000000000000;
    if(select==0)
        begin
        Resultado[30:23]=NumExpB;
        Resultado[22:0]=Res;
        end
    else
        begin
        Resultado[30:23]=NumExpA;
        Resultado[22:0]=Res;
        end
    end       
endmodule
