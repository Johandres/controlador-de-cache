`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.02.2019 23:23:47
// Design Name: 
// Module Name: Deco7Segmentos_CATODE_AND_ANODE_SELECTOR
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Deco7Segmentos_CATODE_AND_ANODE_SELECTOR(
    input wire [3:0] LedIN,
    output logic [7:0] LedOut
   // input wire Mode
    );
    //wire [6:0] uniones;
    Deco7Segmentos DECO7SEG_1(LedIN,LedOut[6:0]);
    always_comb
    begin
    LedOut[7]=1;
    end
   // assign LedOut = (Mode==1)? uniones:~uniones;
endmodule
