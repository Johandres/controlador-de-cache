`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.04.2019 16:08:20
// Design Name: 
// Module Name: Main_SumaFloat
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Main_SumaFloat(
    input logic clk0,
    input logic clk1,
    input logic Mode,
    input logic button,
    input logic buttonAsicr1,
    input logic selectclk,
    input logic IngresarValores,
    input logic [7:0] Variable,
    input logic [1:0] select,
    input logic selectNum,
    output logic [7:0] an,y,
    output logic Led1,Led2,Led3,Led4,cout
    );
logic [7:0] Dezplazamiento;
logic clk,Modos;
logic [2:0] State,NextState;
logic [3:0] out_BCD;
logic [31:0] Resultado, Resultado1;
logic [23:0] s,s1,s2;
logic selectdez,Redondeo,buttonOut,buttonAsicr;
logic [31:0] NumA;
logic [31:0] NumB;

//valores iniciales
initial begin
State=0;
Resultado=0;
cout=0;
 Led1=0;
 Led2=0;
 Led3=0;
 Led4=0;
end

always_comb 
begin
//Selecciono el Modo 0 Clock y Modo 1 button
if(Mode==0)
Modos=clk;
else
Modos=buttonAsicr;
//selecciono la frecuencia del reloj
if(selectclk==1)
clk=clk1;
else
clk=clk0;

end

Debouncer_top Boton2(clk0,buttonAsicr1,buttonAsicr);
//Estado cero
Debouncer_top Boton(clk0,button,buttonOut);
IngresarValores Ingresa(buttonOut,Variable,select,selectNum,NumA,NumB);
//Estado uno
ComparacionExponentes ComparaExp(NumA[30:23],NumB[30:23],Dezplazamiento,selectdez);
//Estado dos
CorrimientoFloat Corrimiento(NumA[22:0],NumB[22:0],selectdez,Dezplazamiento,s,s1,Redondeo);
//Estado tres
CarryLookAhead #(24)Suma1(s,s1,Redondeo,s2,cout); 
//Estado cuatro1
Normalizador Iee(s2[22:0],NumA[30:23],NumB[30:23],selectdez,Resultado1);
//Implementacion a 7 segmentos
altenator_7segments_Nexy4 #(8) ALT(clk0,Resultado,out_BCD,an);
Deco7Segmentos_CATODE_AND_ANODE_SELECTOR Decodificador(out_BCD,y);

always@(State)
begin
case(State)
0: NextState = 1;
1:   NextState = 2;
2:  NextState = 3;
3: NextState = 0;
4: NextState = 0;
endcase
end
//Asignacion 
always @(posedge Modos )
      if(IngresarValores==0)
           begin
           State=NextState;
          end
       else
       State=4;
  // Asignacion de salidas
always @(State)
      if(State == 0)
      begin
      Led1=1;
      Led2=0;
      Led3=0;
      Led4=0;
      Resultado[23:8]=0;
      Resultado[31:24]=NumA[30:23];
      Resultado[7:0]=NumB[30:23];
      end 
      else if(State == 1) 
      begin
      Led1=0;
      Led2=1;
      Led3=0;
      Led4=0;
      Resultado[31:23]=0;
      Resultado[22:0]=s;
      end
      else if(State == 2)
      begin
      Led1=0;
      Led2=0;
      Led3=1;
      Led4=0;
      Resultado[31:23]=0;
      Resultado[22:0]=s2;
      end 
      else if(State == 3)
      begin
      Led1=0;
      Led2=0;
      Led3=0;
      Led4=1;
      Resultado=Resultado1;
      end 
      else
      begin
      Led1=0;
      Led2=0;
      Led3=0;
      Led4=1;
      Resultado=Resultado1;
      end
endmodule
