`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.03.2019 16:36:40
// Design Name: 
// Module Name: Selector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Selector#(parameter nbits=4)(input logic [nbits-1 :0] s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,
input logic c,c1,c2,c3,c5,c6,bcwire,
input logic ALUFLAG1,
input logic [3:0] seleccion,
input logic [nbits-1 :0] a,
input logic [nbits-1 :0] b,
input logic Sign,
output logic [nbits-1:0] s,Swire,
output logic C,signwire,cwire,awire,bwire,swire
    );
always_comb
case(seleccion)
1: begin
    s=s1;
    C=c;
    awire=a[nbits-1];
    bwire=bcwire;
    swire=s1[nbits-1];
    signwire=Sign;   
    Swire=s1;
    cwire=c;    
   end 
2: begin  
    s=s2;
    C=c1;
    awire=a[nbits-1]; 
    bwire=bcwire; 
    swire=s2[nbits-1];
    signwire=Sign;    
    Swire=s2;         
    cwire=c1;             
   end   
     
3: begin
    s=s3;
    C=c6;
    if (ALUFLAG1==0)
    begin
        awire=a[nbits-1]; 
        bwire=0; 
    end
    else
        begin
            awire=0; 
            bwire=b[nbits-1];
        end
    swire=s3[nbits-1];
    signwire=Sign;    
    Swire=s3;         
    cwire=c6; 
    end    
4: begin
    s=s4;
    C=c5;
    if (ALUFLAG1==0)
    begin
        awire=a[nbits-1]; 
        bwire=1; 
    end
    else
        begin
            awire=1; 
            bwire=b[nbits-1];
        end
    swire=s4[nbits-1];
    signwire=Sign;    
    Swire=s4;         
    cwire=c5; 
   end
  
5: begin
    s=s5; 
    Swire=s5; 
    cwire=0;
    awire=0;
    bwire=0;
    swire=0;
    signwire=0;
    C=0;
   end
   
6: begin
    s=s6;
    Swire=s6; 
    cwire=0;
    signwire=0;
    C=0;
    awire=0;
    bwire=0;
    swire=0;
   end
   
7: begin
    s=s7;
    Swire=s7; 
    cwire=0;
    signwire=0;
    C=0;
    awire=0;
    bwire=0;
    swire=0;
   end
   
   
8: begin
    C=0;
    s=s8;
    Swire=s8; 
    cwire=0;
    signwire=0;
    awire=0;
    bwire=0;
    swire=0;
   end
   
   
9: begin
    s=s9;
    C=c3;
    Swire=s9; 
    cwire=c3;
    signwire=0;
    awire=0;
    bwire=0;
    swire=0;
   end
   
   
10:begin    
    s=s10;
    C=c2;
    Swire=s10; 
    cwire=c2;
    signwire=0;
    awire=0;
    bwire=0;
    swire=0;
   end  
   
     
default:
    begin
        s=0;
        C=0;
        awire=0;
        bwire=0;
        cwire=0;
        signwire=0;
        Swire=0;
        swire=0;
    end
endcase
endmodule
