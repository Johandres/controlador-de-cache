`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.03.2019 14:37:19
// Design Name: 
// Module Name: OverflowFLag
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OverflowFlag(
    output wire FlagOverflow,
    input wire AMSB,
    input wire BMSB,
    input wire SMSB,
    input wire SignMode //Es igual a Sign. Identifica si se toma valores con signo(2^(n-1)-1,-2^(n-1)) si esta en 1 o no si est� en 0 (0,2^n)
    );
    assign FlagOverflow = SignMode==0? 0:(~(AMSB|BMSB)&SMSB)|~(~(AMSB&BMSB)|SMSB);
endmodule
