`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.02.2019 12:12:36
// Design Name: 
// Module Name: 7SegmentosDeco
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Deco7Segmentos(
    input wire [3:0] Num,
    output logic [6:0] Led
    );
    logic [6:0] led1;
    always_comb
    begin
    led1 = (Num==4'd0)? 7'b0111111:(Num==4'd1)? 7'b110:(Num==4'd2)? 7'b1011011:(Num==4'd3)? 7'b1001111:(Num==4'd4)? 7'b1100110:(Num==4'd5)? 7'b1101101:(Num==4'd6)? 7'b1111101:(Num==4'd7)? 7'b1000111:(Num==4'd8)? 7'b1111111:(Num==4'd9)? 7'b1101111:(Num==4'd10)? 7'b1110111:(Num==4'd11)? 7'b1111100:(Num==4'd12)? 7'b0111001:(Num==4'd13)? 7'b1011110:(Num==4'd14)? 7'b1111001:7'b1110001;
    Led=~led1;
    end
endmodule
