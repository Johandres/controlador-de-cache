`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2019 01:23:35
// Design Name: 
// Module Name: SelectCache
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SelectCache(
input reg[63:0] ROMC0[0:1023],
input reg[63:0] ROMC1[0:1023],
input reg[63:0] ROMC2[0:1023],
input reg[63:0] ROMC3[0:1023],
input logic [1:0] way,
output reg[63:0] ROMCX[0:1023],
input reg[10:0] TAGC0[0:1023],
input reg[10:0] TAGC1[0:1023],
input reg[10:0] TAGC2[0:1023],
input reg[10:0] TAGC3[0:1023],
output reg[10:0] TAGCX[0:1023]
    );
    reg[63:0] ROMCF[0:1023];
    reg[10:0] TAGCF[0:1023];
    always_comb
    begin
  case (way)
0:begin
    ROMCF=ROMC0;
    TAGCF=TAGC0; 
   end
1:begin
    ROMCF=ROMC1;
    TAGCF=TAGC1;
  end
2:begin
    ROMCF=ROMC2;
    TAGCF=TAGC2;
   end
3:begin
    ROMCF=ROMC3;
    TAGCF=TAGC3;
    end
endcase
end
assign ROMCX=ROMCF;
assign TAGCX=TAGCF;
endmodule
