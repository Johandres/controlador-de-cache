`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.11.2019 00:15:53
// Design Name: 
// Module Name: Cache_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Cache_TB(
    );
//nodos internas
logic [9:0] In_Addres;
logic [63:0] In_Data;
wire [63:0] Out_Data;
logic rw,init;
reg[63:0] ROM[0:1023];
//Dut
Memoria #(.Data(64),.Tam(1024),.Addr(10),.Datafile("Cache0.mem"))Cache(.init(init),.rw (rw),.In_Addres(In_Addres),.In_Data(In_Data),.Out_Data(Out_Data),.ROMIn(ROM),.ROMOut(ROM));
initial
begin
init=1;
rw=0;
In_Addres=10'h000;
#200
rw=1;
In_Addres=10'h000;
In_Data=64'hFFFFFFFFFFF44444;
#200
init=0;
rw=0;
In_Addres=10'h000; //400h =1024
#200
rw=0;
In_Addres=10'h001;
end
endmodule
