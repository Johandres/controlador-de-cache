`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 24.11.2019 16:13:33
// Design Name: 
// Module Name: Comparador
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Comparador(
    input logic [10:0] MP_Addres,
    output logic [9:0] C_Addres,
    input  reg[10:0] TAGCF[0:1023],
    output logic enable
    );
    int i;
    initial
    begin
    i=0;
    while(i<1024)
    begin
    if(TAGCF[i]==MP_Addres)
        begin
        C_Addres=i[9:0];
        enable=1'b1;
        i=1024;
        end
    else
       begin
        enable=1'b0;
        end
    i=i+1;
    end
    end
endmodule
