`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.04.2019 19:54:09
// Design Name: 
// Module Name: ROM12x256
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ROMX #(parameter Data=2,Tam=2,Datafile="Spritedata.mem")(// 32768 espacios de 2 bytes 64KB
    output reg[Data-1:0] ROMOut[0:Tam-1]
    );
    
    reg[Data-1:0] ROM[0:Tam-1];
    assign ROMOut=ROM;   
    initial
    begin
    $readmemh (Datafile,ROM);
    end
    endmodule