`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2019 01:33:31
// Design Name: 
// Module Name: SaveCache
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SaveCache(
output reg[63:0] ROMC0X[0:1023],
output reg[63:0] ROMC1X[0:1023],
output reg[63:0] ROMC2X[0:1023],
output reg[63:0] ROMC3X[0:1023],
input logic [1:0] way,
input reg[63:0] ROMCF[0:1023],
output reg[10:0] TAGC0X[0:1023],
output reg[10:0] TAGC1X[0:1023],
output reg[10:0] TAGC2X[0:1023],
output reg[10:0] TAGC3X[0:1023],
input reg[10:0] TAGCF[0:1023],
input reg[63:0] ROMC0[0:1023],
input reg[63:0] ROMC1[0:1023],
input reg[63:0] ROMC2[0:1023],
input reg[63:0] ROMC3[0:1023],
input reg[10:0] TAGC0[0:1023],
input reg[10:0] TAGC1[0:1023],
input reg[10:0] TAGC2[0:1023],
input reg[10:0] TAGC3[0:1023]
    );
    reg[63:0] ROMC0F[0:1023];
    reg[63:0] ROMC1F[0:1023];
    reg[63:0] ROMC2F[0:1023];
    reg[63:0] ROMC3F[0:1023];
    reg[10:0] TAGC0F[0:1023];
    reg[10:0] TAGC1F[0:1023];
    reg[10:0] TAGC2F[0:1023];
    reg[10:0] TAGC3F[0:1023];
    always_comb
    begin
    case (way)
0:begin
    ROMC0F=ROMCF;
    TAGC0F=TAGCF;
    ROMC1F=ROMC1;
    ROMC2F=ROMC2;
    ROMC3F=ROMC3;
    TAGC1F=TAGC1;
    TAGC2F=TAGC2;
    TAGC3F= TAGC3;
   end
1:begin
    ROMC1F=ROMCF;
    TAGC1F=TAGCF;
    ROMC0F=ROMC0;
    ROMC2F=ROMC2;
    ROMC3F=ROMC3;
    TAGC0F=TAGC0;
    TAGC2F=TAGC2;
    TAGC3F= TAGC3;
  end
2:begin
    ROMC2F=ROMCF;
    TAGC2F=TAGCF;
    ROMC1F=ROMC1;
    ROMC0F=ROMC0;
    ROMC3F=ROMC3;
    TAGC1F=TAGC1;
    TAGC0F=TAGC0;
    TAGC3F= TAGC3;
   end
3:begin
    ROMC3F=ROMCF;
    TAGC3F=TAGCF;
    ROMC1F=ROMC1;
    ROMC2F=ROMC2;
    ROMC0F=ROMC0;
    TAGC1F=TAGC1;
    TAGC2F=TAGC2;
    TAGC0F= TAGC0;
    end
endcase
end
assign ROMC0X=ROMC0F;
assign ROMC1X=ROMC1F;
assign ROMC2X=ROMC2F;
assign ROMC3X=ROMC3F;
assign TAGC0X=TAGC0F;
assign TAGC1X=TAGC1F;
assign TAGC2X=TAGC2F;
assign TAGC3X=TAGC3F;
endmodule