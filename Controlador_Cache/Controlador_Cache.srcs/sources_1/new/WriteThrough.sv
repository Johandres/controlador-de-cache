`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2019 00:05:15
// Design Name: 
// Module Name: WriteThrough
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module WriteThrough(
input wire [14:0] MP_Addres,
input wire [9:0] C_Addres,
input reg[15:0] ROMMP[0:32767],
input reg[63:0] ROMC0[0:1023],
input reg[10:0] TAGC0[0:1023],
output reg[63:0] ROMCF[0:1023],
output  reg[10:0] TAGCF[0:1023]
    );
    wire [10:0] TAGOut;
    wire [63:0] COut_Data,CIN_Data;
    MUXwrite Write(MP_Addres[14:2],CIN_Data,ROMMP);
    MemoriaRW #(.Data(11),.Tam(1024),.Addr(10))TAG0(1'b1,C_Addres,MP_Addres[14:4],TAGOut,TAGC0,TAGCF);
    MemoriaRW #(.Data(64),.Tam(1024),.Addr(10))Cwrite(1'b1,C_Addres,CIN_Data,COut_Data,ROMC0,ROMCF);
endmodule
