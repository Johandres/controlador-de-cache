`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.11.2019 23:22:58
// Design Name: 
// Module Name: Cache_Mem
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ROM1024x64( //1024 espacios de 8 bytes 8KB
    input wire init,
    input wire rw,
    input wire [9:0] In_Addres,
    input wire [63: 0] In_Data,
    output logic [63:0] Out_Data,
    input reg[63:0] ROMIn[0:1023],
    output reg[63:0] ROMOut[0:1023]
    );
    
    parameter Datafile="Spritedata.txt";
     reg[63:0] ROM[0:1023];
     always_comb
     begin
     ROMOut=ROM;
    if (rw == 0)
    Out_Data <= ROM[In_Addres];
    else
     ROM[In_Addres]<=In_Data;
  end   
   
    initial
    begin
    $readmemh (Datafile,ROM);
    if(init==0)
        ROM<=ROMIn;
    end
endmodule
