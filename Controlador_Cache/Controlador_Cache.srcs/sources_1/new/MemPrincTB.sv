`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2019 13:15:07
// Design Name: 
// Module Name: MemPrincTB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MemPrincTB(
);
   //nodos internas
logic [14:0] In_Addres;
logic [15:0] In_Data;
logic [15:0] Out_Data;
logic rw,init;
reg [15:0] ROM[0:32767];
//Dut
Memoria #(.Data(16),.Tam(32768),.Addr(15),.Datafile("MemoriaPrincipal.mem")) MP(.init(init),.rw (rw),.In_Addres(In_Addres),.In_Data(In_Data),.Out_Data(Out_Data),.ROMIn(ROM),.ROMOut(ROM));
initial
begin
init=1;
rw=0;
In_Addres=15'h0000;
#200
init=0;
rw=1;
In_Addres=15'h0000;
In_Data=16'h4444;
#200
rw=0;
In_Addres=15'h0000; //400h =1024
#200
rw=0;
In_Addres=15'h0001;
end
endmodule

