`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2019 16:39:41
// Design Name: 
// Module Name: MemoriaRW
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MemoriaRW #(parameter Data=2,Tam=2,Addr=2)(
    input wire rw,
    input wire [Addr-1:0] In_Addres,
    input wire [Data-1: 0] In_Data,
    output logic [Data-1:0] Out_Data,
    input reg[Data-1:0] ROMIn[0:Tam-1],
    output reg[Data-1:0] ROMOut[0:Tam-1]
    );
     always_comb
     begin
    if (rw == 0)
    Out_Data = ROMIn[In_Addres];
    else
     ROMIn[In_Addres]=In_Data;
     Out_Data =ROMIn[In_Addres];
  end   
  assign ROMOut=ROMIn;
endmodule
