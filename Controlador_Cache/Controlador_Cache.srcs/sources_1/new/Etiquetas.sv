`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.11.2019 14:26:48
// Design Name: 
// Module Name: Etiquetas
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ROM1024x11( //1024 espacios de 1 bytes y 7 bits 1.9KB
    input wire init,
    input wire rw,
    input wire [9:0] In_Addres,
    input wire [10: 0] In_Data,
    output logic [10:0] Out_Data,
    input reg[10:0] ROMIn[0:1023],
    output reg[10:0] ROMOut[0:1023]
    );
    
    parameter Datafile="Spritedata.txt";
     reg[10:0] ROM[0:1023];
     ROM32768x16 #(.Datafile("MemoriaPrincipal.mem"))Inicia(ROM);
   always_comb
   begin
   if(init==1)
        begin
        ROMOut=ROM;
        end
     else
        begin
        ROMOut=ROMIn;
        end
    if (rw == 0)
    Out_Data = ROMOut[In_Addres];
    else
     ROMOut[In_Addres]=In_Data;
     end
endmodule