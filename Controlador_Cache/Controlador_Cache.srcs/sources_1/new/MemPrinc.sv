`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2019 13:09:53
// Design Name: 
// Module Name: MemPrinc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Memoria #(parameter Data=2,Tam=2,Addr=2,Datafile="Memoria.txt")(
    input wire init,
    input wire rw,
    input wire [Addr-1:0] In_Addres,
    input wire [Data-1: 0] In_Data,
    output logic [Data-1:0] Out_Data,
    input reg[Data-1:0] ROMIn[0:Tam-1],
    output reg[Data-1:0] ROMOut[0:Tam-1]
    );
     reg[Data-1:0] ROM[0:Tam-1];
     ROMX #(.Data(Data),.Tam(Tam),.Datafile(Datafile))Inicia(ROM);
     always_comb
     begin
     if(init==1)
        begin
        ROMOut=ROM;
        end
     else
        begin
        ROMOut=ROMIn;
        end
    if (rw == 0)
    Out_Data = ROMOut[In_Addres];
    else
     ROMOut[In_Addres]=In_Data;
  end   
endmodule
