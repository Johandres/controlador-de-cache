`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 24.11.2019 16:01:20
// Design Name: 
// Module Name: Miss_Hit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Miss_Hit(
    input wire [14:0] MP_Addres,
    output wire[15:0] Dato,
    input reg[63:0] ROMC0[0:1023],
    input reg[63:0] ROMC1[0:1023],
    input reg[63:0] ROMC2[0:1023],
    input reg[63:0] ROMC3[0:1023],
    input reg[10:0] TAGC0[0:1023],
    input reg[10:0] TAGC1[0:1023],
    input reg[10:0] TAGC2[0:1023],
    input reg[10:0] TAGC3[0:1023],
    output reg[63:0] ROMC0F[0:1023],
    output reg[63:0] ROMC1F[0:1023],
    output reg[63:0] ROMC2F[0:1023],
    output reg[63:0] ROMC3F[0:1023],
    output reg[10:0] TAGC0F[0:1023],
    output reg[10:0] TAGC1F[0:1023],
    output reg[10:0] TAGC2F[0:1023],
    output reg[10:0] TAGC3F[0:1023]
    );
    reg[63:0] ROMCF[0:1023];
    reg[63:0] ROMCF1[0:1023];
    reg[10:0] TAGCF[0:1023];
    logic [9:0] C_Addres;
    logic enable;
    wire [63:0] COut_Data;
   SelectCache Select1(ROMC0,ROMC1,ROMC2,ROMC3,MP_Addres[3:2],ROMCF,TAGC0,TAGC1,TAGC2,TAGC3,TAGCF);
   Comparador Compare(MP_Addres[14:4],C_Addres,TAGCF,enable);
   MemoriaRW #(.Data(64),.Tam(1024),.Addr(10))Cwread(1'b0,C_Addres,64'h0000000000000000,COut_Data,ROMCF,ROMCF1);
   MUXread read(enable,COut_Data,MP_Addres[1:0],Dato);
   SaveCache Save1(ROMC0F,ROMC1F,ROMC2F,ROMC3F,MP_Addres[3:2],ROMCF,TAGC0F,TAGC1F,TAGC2F,TAGC3F,TAGCF,ROMC0,ROMC1,ROMC2,ROMC3,TAGC0,TAGC1,TAGC2,TAGC3);  
endmodule
