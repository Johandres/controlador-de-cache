`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2019 19:05:34
// Design Name: 
// Module Name: Main1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Main(
    input wire [14:0] MP_Addres,
    input wire [9:0] C_Addres,
    input reg[63:0] ROMC0[0:1023],
    input reg[63:0] ROMC1[0:1023],
    input reg[63:0] ROMC2[0:1023],
    input reg[63:0] ROMC3[0:1023],
    input reg[15:0] ROMMP[0:32767],
    input reg[10:0] TAGC0[0:1023],
    input reg[10:0] TAGC1[0:1023],
    input reg[10:0] TAGC2[0:1023],
    input reg[10:0] TAGC3[0:1023],
    output reg[63:0] ROMC0F[0:1023],
    output reg[63:0] ROMC1F[0:1023],
    output reg[63:0] ROMC2F[0:1023],
    output reg[63:0] ROMC3F[0:1023],
    output reg[10:0] TAGC0F[0:1023],
    output reg[10:0] TAGC1F[0:1023],
    output reg[10:0] TAGC2F[0:1023],
    output reg[10:0] TAGC3F[0:1023]
    );
    reg[63:0] ROMCF[0:1023];
    reg[63:0] ROMCFF[0:1023];
    reg[10:0] TAGCF[0:1023];
    reg[10:0] TAGCFF[0:1023];
   SelectCache Select(ROMC0,ROMC1,ROMC2,ROMC3,MP_Addres[3:2],ROMCF,TAGC0,TAGC1,TAGC2,TAGC3,TAGCF);
   WriteThrough Escribir(MP_Addres,C_Addres,ROMMP,ROMCF,TAGCF,ROMCFF,TAGCFF);
   SaveCache Save(ROMC0F,ROMC1F,ROMC2F,ROMC3F,MP_Addres[3:2],ROMCFF,TAGC0F,TAGC1F,TAGC2F,TAGC3F,TAGCFF,ROMC0,ROMC1,ROMC2,ROMC3,TAGC0,TAGC1,TAGC2,TAGC3);  
endmodule
