`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 24.11.2019 22:08:24
// Design Name: 
// Module Name: Contador
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module Contador1024(output reg[9:0] data,input wire reset);
//-- Sensible al flanco de subida
always_comb
begin
  data = data + 1;
 if (data==10'd1023)
    data=10'd0;
 if(reset==1)
    data=10'd0;
end
initial
begin
data=10'd0;
end
endmodule
