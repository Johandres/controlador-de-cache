`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2019 00:01:54
// Design Name: 
// Module Name: MuxRead
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MUXread(
    input logic enable,
    input logic [63:0] DatoCache,
    input logic [1:0] pos1,
    output logic [15:0] DatoF
    );
    logic [15:0] DatoF1;
    always_comb
    begin
    if(enable==1)
    begin
    case (pos1)
0:DatoF1=DatoCache[15:0];

1:DatoF1=DatoCache[31:16];

2:DatoF1=DatoCache[47:32];

3:DatoF1=DatoCache[63:48];
endcase
    end
  end
  assign DatoF=DatoF1;
endmodule

