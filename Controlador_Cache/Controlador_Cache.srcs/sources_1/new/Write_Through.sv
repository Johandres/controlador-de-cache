`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.11.2019 23:20:15
// Design Name: 
// Module Name: Write_Through
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////////////
module MUXwrite(
    input [12:0] MpAddr,
    output logic [63:0] DatoCacheF,
    input reg[15:0] ROMMP[0:32767]
    );
    reg[15:0] ROMMF[0:32767];
    reg[15:0] ROMMF1[0:32767];
    reg[15:0] ROMMF2[0:32767];
    reg[15:0] ROMMF3[0:32767];
    logic [15:0] DatoMP0,DatoMP1,DatoMP2,DatoMP3;
    MemoriaRW #(.Data(16),.Tam(32768),.Addr(15))MPread0(1'b0,{MpAddr,2'b00},16'h0000,DatoMP0,ROMMP,ROMMF);
    MemoriaRW #(.Data(16),.Tam(32768),.Addr(15))MPread1(1'b0,{MpAddr,2'b01},16'h0000,DatoMP1,ROMMP,ROMMF1);
    MemoriaRW #(.Data(16),.Tam(32768),.Addr(15))MPread2(1'b0,{MpAddr,2'b10},16'h0000,DatoMP2,ROMMP,ROMMF2);
    MemoriaRW #(.Data(16),.Tam(32768),.Addr(15))MPread3(1'b0,{MpAddr,2'b11},16'h0000,DatoMP3,ROMMP,ROMMF3);
    always_comb
    begin
    DatoCacheF[15:0]=DatoMP0;
    DatoCacheF[31:16]=DatoMP1;
    DatoCacheF[47:32]=DatoMP2;
    DatoCacheF[63:48]=DatoMP3;
  end
endmodule
