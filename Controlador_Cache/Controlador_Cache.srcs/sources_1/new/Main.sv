`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.11.2019 23:05:01
// Design Name: 
// Module Name: Main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Inicia(
    input wire init,
    input wire [14:0] MP_Addres,
    input wire [9:0] C_Addres,
    input reg[63:0] ROMC0[0:1023],
    input reg[63:0] ROMC1[0:1023],
    input reg[63:0] ROMC2[0:1023],
    input reg[63:0] ROMC3[0:1023],
    output reg[15:0] ROMMP[0:32767],
    input reg[10:0] TAGC0[0:1023],
    input reg[10:0] TAGC1[0:1023],
    input reg[10:0] TAGC2[0:1023],
    input reg[10:0] TAGC3[0:1023],
    output reg[63:0] ROMC0F[0:1023],
    output reg[63:0] ROMC1F[0:1023],
    output reg[63:0] ROMC2F[0:1023],
    output reg[63:0] ROMC3F[0:1023],
    output reg[10:0] TAGC0F[0:1023],
    output reg[10:0] TAGC1F[0:1023],
    output reg[10:0] TAGC2F[0:1023],
    output reg[10:0] TAGC3F[0:1023]
    );
    wire [63:0] C0Out_Data,C1Out_Data,C2Out_Data,C3Out_Data;
    wire [10:0] T0Out_Data,T1Out_Data,T2Out_Data,T3Out_Data;
    wire [15:0] MPOut_Data;
   Memoria #(.Data(16),.Tam(32768),.Addr(15),.Datafile("MemoriaPrincipal.mem"))MemoriaPrincipal(1'b1,1'b0,15'h0000,16'h0000,MPOut_Data,ROMMP,ROMMP);
   Memoria #(.Data(64),.Tam(1024),.Addr(10),.Datafile("Cache0.mem"))Cache0(init,1'b0,10'h000,64'h0000000000000000,C0Out_Data,ROMC0,ROMC0F);
   Memoria #(.Data(64),.Tam(1024),.Addr(10),.Datafile("Cache0.mem"))Cache1(init,1'b0,10'h000,64'h0000000000000000,C1Out_Data,ROMC1,ROMC1F);
   Memoria #(.Data(64),.Tam(1024),.Addr(10),.Datafile("Cache0.mem"))Cache2(init,1'b0,10'h000,64'h0000000000000000,C2Out_Data,ROMC2,ROMC2F);
   Memoria #(.Data(64),.Tam(1024),.Addr(10),.Datafile("Cache0.mem"))Cache3(init,1'b0,10'h000,64'h0000000000000000,C3Out_Data,ROMC3,ROMC3F);
   Memoria #(.Data(11),.Tam(1024),.Addr(10),.Datafile("TAG0.mem"))TAG0(init,1'b0,10'h000,11'h000,T0Out_Data,TAGC0,TAGC0F);
   Memoria #(.Data(11),.Tam(1024),.Addr(10),.Datafile("TAG0.mem"))TAG1(init,1'b0,10'h000,11'h000,T1Out_Data,TAGC1,TAGC1F);
   Memoria #(.Data(11),.Tam(1024),.Addr(10),.Datafile("TAG0.mem"))TAG2(init,1'b0,10'h000,11'h000,T2Out_Data,TAGC2,TAGC2F);
   Memoria #(.Data(11),.Tam(1024),.Addr(10),.Datafile("TAG0.mem"))TAG3(init,1'b0,10'h000,11'h000,T3Out_Data,TAGC3,TAGC3F);
   
endmodule
