`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.11.2019 17:53:45
// Design Name: 
// Module Name: MainTB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MainTB(
 );
//nodos internas
logic init;
logic [14:0] MP_Addres;
logic [9:0] C_Addres;
wire[63:0] ROMC0[0:1023] ;
reg[63:0] ROMC1[0:1023] ;
reg[63:0] ROMC2[0:1023] ;
reg[63:0] ROMC3[0:1023] ;
reg[15:0] ROMMP[0:32767];
reg[10:0] TAGC0[0:1023] ;
reg[10:0] TAGC1[0:1023] ;
reg[10:0] TAGC2[0:1023] ;
reg[10:0] TAGC3[0:1023] ;
reg[63:0] ROMC0F[0:1023] ;
reg[63:0] ROMC1F[0:1023] ;
reg[63:0] ROMC2F[0:1023] ;
reg[63:0] ROMC3F[0:1023] ;
reg[10:0] TAGC0F[0:1023] ;
reg[10:0] TAGC1F[0:1023] ;
reg[10:0] TAGC2F[0:1023] ;
reg[10:0] TAGC3F[0:1023] ;
Inicia test1(.init(init),.MP_Addres(15'h0000),.C_Addres(10'h000),.ROMC0(ROMC0),.ROMC1(ROMC1),.ROMC2(ROMC2),.ROMC3(ROMC3),.ROMMP(ROMMP),.TAGC0(TAGC0),.TAGC1(TAGC1),.TAGC2(TAGC2),.TAGC3(TAGC3),.ROMC0F(ROMC0F),.ROMC1F(ROMC1F),.ROMC2F(ROMC2F),.ROMC3F(ROMC3F),.TAGC0F(TAGC0F),.TAGC1F(TAGC1F),.TAGC2F(TAGC2F),.TAGC3F(TAGC3F));
Main Escritura(.MP_Addres(MP_Addres),.C_Addres(C_Addres),.ROMC0(ROMC0F),.ROMC1(ROMC1F),.ROMC2(ROMC2F),.ROMC3(ROMC3F),.ROMMP(ROMMP),.TAGC0(TAGC0F),.TAGC1(TAGC1F),.TAGC2(TAGC2F),.TAGC3(TAGC3F),.ROMC0F(ROMC0),.ROMC1F(ROMC1),.ROMC2F(ROMC2),.ROMC3F(ROMC3),.TAGC0F(TAGC0),.TAGC1F(TAGC1),.TAGC2F(TAGC2),.TAGC3F(TAGC3));
initial
begin
init=1;
MP_Addres=16'h0010;
C_Addres=10'h000;
#50
init=0;
MP_Addres=16'h0014;
C_Addres=10'h000;
#50
init=0;
MP_Addres=16'h0018;
C_Addres=10'h000;
#50
init=0;
MP_Addres=16'h001C;
C_Addres=10'h000;
end
endmodule
